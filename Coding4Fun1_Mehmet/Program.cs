﻿using System;

namespace Coding4Fun1_Mehmet
{
//    Big and Small Bottle (av Hanna & Amanda)

//Skapa 2 klasser, “BigBottle” och “SmallBottle”.
//Båda klasserna innehåller en property.
//Gör en metod som skriver ut hur mycket en flaska innehåller.
//Skriv ut på konsolen hur mycket flaskorna innehåller.
//BigBottle ska nu typkonverteras både till och från SmallBottle, obs den lilla flaskan ska fortfarande innehålla 33cl.
//Skriv ut hur mycket flaskorna innehåller efter konverteringen.
 

//Utskriften ska se ut såhär:
//The big bottle contains 100cl.
//The small bottle contains 33cl.
//The small bottle contains 33cl.
//The big bottle contains 33cl.

//Lycka Till =)
    public class BigBottle
    {
        public uint Liquid { get; set; }

        public void Contains()
        {
            Console.WriteLine("The big bottle contains {0}cl.", Liquid);
        }
        public static implicit operator BigBottle(SmallBottle sb)
        {
            BigBottle bb = new BigBottle();
            bb.Liquid = sb.liquid;
            return bb;
        }
        public static explicit operator SmallBottle(BigBottle bb)
        {
            SmallBottle sb = new SmallBottle();
            sb.liquid = bb.Liquid;
            return sb;
        }
    }
    public class SmallBottle
    {
        private uint _liquid;
        public uint liquid
        {
            get { return this._liquid; }
            set
            {
                if (value <= 33)
                    _liquid = value;
                else
                    _liquid = 33;

            }
        }

        public void Contains()
        {

            Console.WriteLine("The small bottle contains {0}cl.", liquid);


        }
    }
    class Program
    {
//Hej!

//Här är den uppgift som jag vill att ni försöker er på.

//Skriv ett program som först läser en starttid (två tal, timmar och minuter, till exempel 12 41)
//och därefter en sluttid (två andra tal, till exempel 16 28)
//och därefter beräknar och skriver ut hur lång tid det är mellan tiderna. 
//Du kan förutsätta att sluttiden är större än starttiden. Programmet skall klara av 
//att hantera att start- och sluttid ligger på var sin sida midnatt men man kan 
//förutsätta att skillnaden mellan start- och sluttid aldrig blir större än 24 timmar.


//Lycka till.

///Thomas
        public static void TimeDiff(string time1, string time2)
        {
            int hour1 = int.Parse(time1.Substring(0, 2));
            int min1 = int.Parse(time1.Substring(3, 2));

            int hour2 = int.Parse(time2.Substring(0, 2));
            int min2 = int.Parse(time2.Substring(3, 2));

            string diffHour = (((hour1*60 + min1) - (hour2*60 + min2) - (((hour1*60 + min1) - (hour2*60 + min2))%60))/60).ToString();
            string diffMin = (((hour1*60 + min1) - (hour2*60 + min2))%60).ToString();

            Console.WriteLine("\nDifference between these two times is: {0} hours {1} minutes", diffHour,diffMin);
        }


        static void Main()
        {
            #region Bottles
            BigBottle bb = new BigBottle() { Liquid = 100 };
            SmallBottle sb = new SmallBottle() { liquid = 40 };

            bb.Contains();
            sb.Contains();

            SmallBottle sb1 = (SmallBottle)bb;
            sb1.Contains();

            BigBottle bb1 = sb;
            bb1.Contains();

            Console.ReadLine();
            #endregion Bottles

#region Thomas Upggift
            Console.Write("Enter the first time in hh:mm format and press Enter: ");
            string time1 = Console.ReadLine();
            Console.Write("\nEnter the second time in hh:mm format and press Enter: ");
            string time2 = Console.ReadLine();

            TimeDiff(time1,time2);
            Console.ReadKey();
#endregion Thomas Uppgift



        }
    }

}